import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  show = false;
  clicks = [];
  counter = 0;

  onToggle() {
    this.clicks.push(Date.now());
    return this.show === false ? this.show = true : this.show = false;
  }

  getCounter(i) {
    return i >= 4 ? "blue" : "white";
  }
}
